﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public static class CombatSystem {
	public static void Next (Battle battle) {
		battle.combatants.Sort((c1, c2) => c1.waitTime.CompareTo(c2.waitTime));
		battle.defender.waitTime -= battle.attacker.waitTime;
		battle.attacker.waitTime = 0;
	}

	public static Move PickMove (Battle battle) {
		var pokemon = battle.attacker.CurrentPokemon;
		return (pokemon.energy >= Mathf.Abs(pokemon.ChargeMove.energy)) ? pokemon.ChargeMove : pokemon.FastMove;
	}

	public static void ApplyMove (Battle battle) {
		var attacker = battle.attacker;
		var defender = battle.defender;
		var move = battle.move;
		var attack = attacker.CurrentPokemon.Attack;
		var defense = defender.CurrentPokemon.Defense;
		var stab = GetSameTypeAttackBonus(move, attacker.CurrentPokemon.Stats);
		var weakness = GetTypeMultiplier(move, defender.CurrentPokemon.Stats);
		var damage = Mathf.Floor(0.5f * attack / defense * move.power * stab * weakness) + 1;

		battle.lastDamage = (int)damage;
		defender.CurrentPokemon.hitPoints = (int)Mathf.Max(0, defender.CurrentPokemon.hitPoints - damage);
		attacker.CurrentPokemon.energy = (int)Mathf.Min(attacker.CurrentPokemon.energy + move.energy, 100);
		attacker.waitTime += move.duration;
	}

	public static bool SwapIfNeeded (Battle battle) {
		var defender = battle.defender;
		if (defender.CurrentPokemon.hitPoints == 0) {
			if (defender.currentPokemonIndex + 1 < defender.pokemon.Count) {
				defender.currentPokemonIndex++;
				defender.waitTime = defender.CurrentPokemon.FastMove.duration;
				return true;
			}
		}
		return false;
	}

	static float GetSameTypeAttackBonus (Move move, SpeciesStats attacker) {
		return (move.type == attacker.typeA || move.type == attacker.typeB) ? 1.25f : 1f;
	}

	static float GetTypeMultiplier (Move move, SpeciesStats defender) {
		double multiplier = 1;
		var connection = DataController.instance.pokemonDatabase.connection;
		var typeMultiplier = connection.Table<TypeMultiplier>()
			.Where(x => x.attack_type_id == move.type && x.defend_type_id == defender.typeA)
			.FirstOrDefault();

		if (typeMultiplier != null) {
			multiplier = typeMultiplier.value;
		}

		if (defender.typeA != defender.typeB) {
			typeMultiplier = connection.Table<TypeMultiplier>()
				.Where(x => x.attack_type_id == move.type && x.defend_type_id == defender.typeB)
				.FirstOrDefault();

			if (typeMultiplier != null) {
				multiplier *= typeMultiplier.value;
			}
		}

		return (float)multiplier;
	}
}
