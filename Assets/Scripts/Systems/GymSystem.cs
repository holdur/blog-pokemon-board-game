﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public static class GymSystem {

	public static Gym GetCurrentGym (Game game, Board board) {
		int tileIndex = game.CurrentPlayer.tileIndex;

		GymSite site = board.tiles[tileIndex].GetComponent<GymSite>();
		if (site == null)
			return null;

		Gym gym = game.gyms[site.index];
		return gym;
	}

	public static void Reset (Gym gym) {
		foreach (Pokemon pokemon in gym.pokemon) {
			HealSystem.FullHeal(pokemon);
			pokemon.energy = 0;
		}
	}

	public static Sprite GetBadge (this Gym gym) {
		return GetBadge(gym.type);
	}

	public static Sprite GetBadge (string type) {
		string fileName = string.Format("Types/Badge{0}", type);
		Sprite sprite = Resources.Load<Sprite>(fileName);
		return sprite;
	}

	public static void AwardBadge (Player player, Gym gym) {
		player.badges.Add (gym.type);
	}

	public static bool CanChallenge (Player player, Gym gym) {
		if (player.badges.Contains (gym.type)) {
			return false;
		}
		return HasTeamSize (player, gym.pokemon.Count);
	}

	static bool HasTeamSize (Player player, int requiredSize) {
		int teamSize = 0;
		foreach (Pokemon p in player.pokemon) {
			if (p.hitPoints > 0)
				teamSize++;
		}
		return teamSize >= requiredSize;
	}
}