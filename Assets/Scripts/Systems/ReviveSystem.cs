﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ReviveSystem {

	public static bool CanApply (Player player, Pokemon pokemon) {
		return (player.revives > 0 && pokemon.hitPoints == 0);
	}

	public static void Apply (Player player, Pokemon pokemon) {
		HealSystem.Heal (pokemon, 1);
		player.revives--;
	}
}
