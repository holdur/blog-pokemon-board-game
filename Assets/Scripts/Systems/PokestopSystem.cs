﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PokestopSystem {

	static List<SupplyAward> AwardPool {
		get {
			if (_awardPool == null) {
				_awardPool = new List<SupplyAward>();
				_awardPool.Add( new SupplyAward(0.5, "Pokeball", delegate(Player player) { player.pokeballs++; }) );
				_awardPool.Add( new SupplyAward(0.25, "Potion", delegate(Player player) { player.potions++; }) );
				_awardPool.Add( new SupplyAward(0.15, "Revive", delegate(Player player) { player.revives++; }) );

				foreach (SupplyAward award in _awardPool) {
					wheel += award.chance;
				}
			}
			return _awardPool;
		}
	}
	static List<SupplyAward> _awardPool;
	static double wheel;

	public static List<SupplyAward> GetRefreshments (Game game, Board board) {
		var currentPlayer = game.CurrentPlayer;
		int tileIndex = currentPlayer.tileIndex;
		if (board.tiles[tileIndex].GetComponent<Pokestop>() == null)
			return null;

		int numberOfAwards = UnityEngine.Random.Range(3, 8);
		List<SupplyAward> toGive = new List<SupplyAward>(numberOfAwards);
		for (int i = 0; i < numberOfAwards; ++i) {
			SupplyAward award = RandomAward();
			toGive.Add(award);
			award.apply(currentPlayer);
		}
		return toGive;
	}

	static SupplyAward RandomAward () {
		double spin = UnityEngine.Random.value * wheel;
		double sum = 0;
		foreach (SupplyAward award in AwardPool) {
			sum += award.chance;
			if (sum >= spin)
				return award;
		}
		return AwardPool.First();
	}

	public static string ToString (List<SupplyAward> awards) {
		Dictionary<string, int> sorted = new Dictionary<string, int> ();
		foreach (SupplyAward award in awards) {
			int count = sorted.ContainsKey (award.award) ? sorted [award.award] : 0;
			sorted [award.award] = count + 1;
		}
		var items = new string[sorted.Keys.Count];
		int index = 0;
		foreach (string key in sorted.Keys) {
			int count = sorted [key];
			items [index] = string.Format ("{0}x{1}", key, count);
			index++;
		}
		return string.Join (", ", items);
	}
}