﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CaptureSystem {
	public static void Capture (Player player, Pokemon pokemon) {
		player.pokeballs--;
		player.candies += 25;
		player.pokemon.Add (pokemon);
		HealSystem.FullHeal(pokemon);
	}

	public static bool TryEscape (Pokemon pokemon) {
		var chance = UnityEngine.Random.value - EasingEquations.EaseInOutCubic(0f, 1f, pokemon.HPRatio);
		return chance < 0;
	}
}