﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PokeCenterSystem {

	public static bool HealPokemon (Game game, Board board) {
		var currentTile = board.tiles[game.CurrentPlayer.tileIndex];
		if (currentTile.GetComponent<PokeCenter>() == null)
			return false;

		// Award candies just for visiting
		game.CurrentPlayer.candies += 100;

		bool didApply = false;
		foreach (Pokemon pokemon in game.CurrentPlayer.pokemon) {
			
			if (pokemon.hitPoints < pokemon.maxHitPoints) {
				HealSystem.FullHeal (pokemon);
				didApply = true;
			}
		}

		return didApply;
	}
}