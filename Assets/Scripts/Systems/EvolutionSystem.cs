﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public class EvolutionSystem {
	public static bool CanApply (Player player, Pokemon pokemon) {
		Evolvable evolvable = pokemon.Evolvable;
		return evolvable != null && evolvable.cost <= player.candies;
	}

	public static int GetCost (Pokemon pokemon) {
		Evolvable evolvable = pokemon.Evolvable;
		return evolvable != null ? evolvable.cost : 0;
	}

	public static void Apply (Player player, Pokemon pokemon) {
		Evolvable evolvable = pokemon.Evolvable;
		player.candies -= evolvable.cost;
		Entity target = DataController.instance.pokemonDatabase.connection.Table<Entity>()
			.Where(x => x.id == evolvable.entity_id)
			.FirstOrDefault();

		pokemon.SetEntity (target);
		pokemon.SetMoves ();
		pokemon.SetLevel (pokemon.level);
		pokemon.hitPoints = pokemon.maxHitPoints;
	}
}
