﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;
using System.Text;

public class PokemonDatabaseDemo : MonoBehaviour {

	void Start () {
		DataController.instance.Load (DoStuff);
	}

	void DoStuff () {
		var connection = DataController.instance.pokemonDatabase.connection;

		var table = connection.Table<Entity> ();
		Debug.Log ("Found items " + table.Count().ToString());

		foreach (Entity entity in table) {
			StringBuilder sb = new StringBuilder ();
			sb.AppendLine ("Loaded entity named: " + entity.label);

			var encounterable = entity.GetEncounterable ();
			if (encounterable != null) {
				sb.AppendLine ("encounterable rate: " + encounterable.rate);
			} else {
				sb.AppendLine ("not encounterable");
			}

			var evolvable = entity.GetEvolvable ();
			if (evolvable != null) {
				sb.AppendFormat ("evolves into: {0}, with a cost of: {1}\n", 
					evolvable.entity_id, evolvable.cost);
			} else {
				sb.AppendLine ("can't evolve");
			}

			var moves = entity.GetMoves ();
			sb.AppendLine ("Can use the following moves:");
			foreach (Move m in moves) {
				sb.AppendLine ("   " + m.name);
			}

			var stats = entity.GetSpeciesStats ();
			sb.AppendFormat ("Attack: {0}, Defense: {1}, Stamina: {2}\n", stats.attack, stats.defense, stats.stamina);

			var type1 = stats.GetPrimaryType ();
			sb.AppendLine ("Type 1: " + type1.name);

			var type2 = stats.GetSecondaryType ();
			sb.AppendLine ("Type 2: " + type2.name);

			Debug.Log (sb.ToString ());
		}
		Debug.Log ("Done");
	}
}
