﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class SupplyAward {
	public readonly double chance;
	public readonly string award;
	public readonly Action<Player> apply;

	public SupplyAward (double chance, string award, Action<Player> apply) {
		this.chance = chance;
		this.award = award;
		this.apply = apply;
	}
}
