﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Gym {
	public string type;
	public List<Pokemon> pokemon = new List<Pokemon>();
}
