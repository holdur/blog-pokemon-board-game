﻿using UnityEngine;
using System.Collections;

namespace TableViewHelpers
{
	public interface IContainer
	{
		IFlow Flow { get; }
		void AutoSize ();
	}
}