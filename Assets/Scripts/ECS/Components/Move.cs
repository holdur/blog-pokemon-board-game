﻿using SQLite4Unity3d;
using System.Collections.Generic;

namespace ECS {
	public class Move {
		public const int ComponentID = 4;

		[PrimaryKey, AutoIncrement]
		public int id { get; set; }
		public string name { get; set; }
		public int type { get; set; }
		public int power { get; set; }
		public double duration { get; set; }
		public int energy { get; set; }
	}

	public static class MoveExtensions {
		public static List<Move> GetMoves (this Entity entity) {
			var connection = DataController.instance.pokemonDatabase.connection;
			var entityComponents = connection.Table<EntityComponent>()
				.Where(x => x.entity_id == entity.id && x.component_id == Move.ComponentID);
			if (entityComponents == null) return null;
			List<Move> retValue = new List<Move>();
			foreach (EntityComponent ec in entityComponents) {
				var component = connection.Table<Move>()
					.Where(x => x.id == ec.component_data_id)
					.FirstOrDefault();
				if (component != null)
					retValue.Add(component);
			}
			return retValue;
		}

		public static Entity GetEntity (this Move item) {
			var connection = DataController.instance.pokemonDatabase.connection;
			var entityComponent = connection.Table<EntityComponent>()
				.Where(x => x.component_id == Move.ComponentID && x.component_data_id == item.id)
				.FirstOrDefault();
			var entity = connection.Table<Entity>()
				.Where(x => x.id == entityComponent.entity_id)
				.FirstOrDefault();
			return entity;
		}
	}
}