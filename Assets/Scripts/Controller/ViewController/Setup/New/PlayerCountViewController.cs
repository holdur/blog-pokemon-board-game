﻿using UnityEngine;
using System;

public class PlayerCountViewController : MonoBehaviour {

	public Action<int> didComplete;

	public void SetPlayerCount (int count) {
		if (didComplete != null)
			didComplete(count);
	}
}