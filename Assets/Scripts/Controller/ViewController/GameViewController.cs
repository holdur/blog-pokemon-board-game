﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameViewController : BaseViewController {

	#region Fields
	public Action moveComplete;

	public Transform cameraRig;
	public Board boardPrefab;
	public SetPooler pawnPool;
	public List<Pawn> pawns = new List<Pawn>();

	const float moveDuration = 0.5f;
	#endregion

	#region MonoBehaviour
	void Awake () {
		DataController.instance.board = Instantiate(boardPrefab).GetComponent<Board>();
	}
	#endregion

	#region Public
	public void LoadGame () {
		LoadPlayers ();
		LoadGyms ();
		NextPlayer ();
	}

	public void NextPlayer () {
		Pawn currentPawn = pawns[game.currentPlayerIndex];
		cameraRig.ResetParent(currentPawn.transform);
	}

	public void MovePawn () {
		StartCoroutine(MoveSequence());
	}

	public void UpdateBuddy (int index) {
		var player = game.players [index];
		if (player.pokemon.Count == 0)
			return;
		var pokemon = game.players[index].pokemon[0];
		var pawn = pawns[index];
		pawn.spriteRenderer.sprite = pokemon.GetAvatar();
		var yPos = GetMinYPos(pawn.spriteRenderer.sprite);
		pawn.spriteRenderer.transform.localPosition = new Vector3(0, -yPos, 0);
	}
	#endregion

	#region Private
	void SetPlayerCount (int count) {
		ClearPawns();
		for (int i = 0; i < count; ++i) {
			AddPawn ();
		}
	}

	void LoadPlayers () {
		SetPlayerCount (game.players.Count);
		for (int i = 0; i < game.players.Count; ++i) {
			UpdateBuddy (i);
			var location = game.players [i].tileIndex;
			pawns[i].transform.ResetParent(board.tiles[location]);
		}
	}

	void LoadGyms () {
		var gymSites = board.GetComponentsInChildren<GymSite> ();
		for (int i = 0; i < gymSites.Length; ++i) {
			var gymSite = gymSites [i];
			var gym = game.gyms[gymSite.index];
			gymSite.Refresh(gym);
		}
	}

	void ClearPawns () {
		cameraRig.ResetParent (null);
		pawnPool.EnqueueAll();
		pawns.Clear();
	}

	void AddPawn () {
		Poolable item = pawnPool.Dequeue();
		item.gameObject.SetActive (true);
		item.transform.ResetParent(board.tiles[0]);
		pawns.Add(item.GetComponent<Pawn>());
	}

	float GetMinYPos (Sprite sprite) {
		float minY = 0;
		foreach (Vector2 vec in sprite.vertices) {
			minY = Mathf.Min(vec.y, minY);
		}
		return minY;
	}

	IEnumerator MoveSequence () {
		var currentPlayer = game.CurrentPlayer;
		var currentPawn = pawns[game.currentPlayerIndex];
		int nextIndex = (currentPlayer.tileIndex + 1) % board.tiles.Count;

		Transform nextTile = board.tiles[nextIndex];
		currentPawn.transform.SetParent(nextTile, true);
		currentPawn.transform.MoveToLocal(Vector3.zero, moveDuration, EasingEquations.Linear);

		Tweener tweener = currentPawn.jumper.MoveToLocal(new Vector3(0, 0.5f, 0), moveDuration / 2.0f, EasingEquations.EaseOutCubic);
		while (tweener.IsPlaying)
			yield return null;

		tweener = currentPawn.jumper.MoveToLocal(Vector3.zero, moveDuration / 2.0f, EasingEquations.EaseInCubic);
		while (tweener.IsPlaying)
			yield return null;

		currentPlayer.tileIndex = nextIndex;

		if (moveComplete != null)
			moveComplete();
	}
	#endregion
}