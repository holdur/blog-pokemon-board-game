﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using ECS;

public class TeamViewController : BaseViewController {

	public Action didComplete;

	[SerializeField] Image avatarImage;
	[SerializeField] Text speciesLabel;
	[SerializeField] Text cpLabel;
	[SerializeField] Text levelLabel;
	[SerializeField] Text healthLabel;
	[SerializeField] Text candiesLabel;
	[SerializeField] Text powerUpCostLabel;
	[SerializeField] Text evolutionCostLabel;
	[SerializeField] Text potionsLabel;
	[SerializeField] Text revivesLabel;
	[SerializeField] Text pageLabel;
	[SerializeField] Button powerUpButton;
	[SerializeField] Button evolveButton;
	[SerializeField] Button healButton;
	[SerializeField] Button reviveButton;
	[SerializeField] Button buddyButton;

	int pokemonIndex;
	public GameViewController gameViewController;
	Pokemon CurrentPokemon { get { return currentPlayer.pokemon[pokemonIndex]; }}
	int PokemonCount { get { return currentPlayer.pokemon.Count; }}

	void OnEnable () {
		gameViewController = GetComponentInParent<FlowController> ().gameViewController;
		pokemonIndex = 0;
		Display();
	}

	public void NextButtonPressed () {
		pokemonIndex = (pokemonIndex + 1) % PokemonCount;
		Display();
	}

	public void PreviousButtonPressed () {
		pokemonIndex = (pokemonIndex - 1 + PokemonCount) % PokemonCount;
		Display();
	}

	public void QuitButtonPressed () {
		if (didComplete != null)
			didComplete();
	}

	public void PowerUpButtonPressed () {
		PowerUpSystem.Apply(currentPlayer, CurrentPokemon);
		Display();
	}

	public void EvolveButtonPressed () {
		EvolutionSystem.Apply(currentPlayer, CurrentPokemon);
		gameViewController.UpdateBuddy(game.currentPlayerIndex);
		Display();
	}

	public void HealButtonPressed () {
		PotionSystem.Apply(currentPlayer, CurrentPokemon);
		Display();
	}

	public void ReviveButtonPressed () {
		ReviveSystem.Apply(currentPlayer, CurrentPokemon);
		Display();
	}

	public void BuddyButtonPressed () {
		Pokemon current = currentPlayer.pokemon[pokemonIndex];
		currentPlayer.pokemon.RemoveAt(pokemonIndex);
		currentPlayer.pokemon.Insert(0, current);
		pokemonIndex = 0;
		gameViewController.UpdateBuddy(game.currentPlayerIndex);
		Display();
	}

	void Display () {

		avatarImage.sprite = CurrentPokemon.GetAvatar();
		speciesLabel.text = CurrentPokemon.Entity.label;
		cpLabel.text = string.Format("CP: {0}", CurrentPokemon.CP);
		levelLabel.text = string.Format("Level: {0}", (CurrentPokemon.level + 1));
		healthLabel.text = string.Format("HP: {0}/{1}", CurrentPokemon.hitPoints, CurrentPokemon.maxHitPoints);

		candiesLabel.text = string.Format ("Candies: {0}", currentPlayer.candies);
		evolveButton.interactable = EvolutionSystem.CanApply (currentPlayer, CurrentPokemon);
		evolutionCostLabel.text = string.Format ("Evolve ({0})", EvolutionSystem.GetCost (CurrentPokemon));

		powerUpButton.interactable = PowerUpSystem.CanApply (currentPlayer, CurrentPokemon);
		powerUpCostLabel.text = string.Format ("Power ({0})", PowerUpSystem.GetCost (CurrentPokemon));

		potionsLabel.text = string.Format("Potions: {0}", currentPlayer.potions);
		healButton.interactable = PotionSystem.CanApply(currentPlayer, CurrentPokemon);

		revivesLabel.text = string.Format("Revives: {0}", currentPlayer.revives);
		reviveButton.interactable = ReviveSystem.CanApply(currentPlayer, CurrentPokemon);

		buddyButton.interactable = pokemonIndex != 0;
		pageLabel.text = string.Format ("{0} of {1}", (pokemonIndex + 1), PokemonCount);
	}
}