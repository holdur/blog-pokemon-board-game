﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AttackTeamViewController : BaseViewController {

	public Action<Battle> didFinish;

	[SerializeField] AttackCandidateView[] candidateViews;
	[SerializeField] Button confirmButton;
	[SerializeField] Button cancelButton;
	List<Pokemon> candidates;
	List<Pokemon> team;

	void OnEnable () {
		candidates = new List<Pokemon> ();
		foreach (Pokemon p in currentPlayer.pokemon) {
			if (p.hitPoints > 0) {
				candidates.Add (p);
			}
		}
		team = new List<Pokemon> (candidateViews.Length);
		for (int i = 0; i < candidateViews.Length; ++i) {
			team.Add (candidates [i]);
			candidateViews [i].Display (candidates [i]);
		}
	}

	protected override void DidShow (Action complete) {
		base.DidShow (complete);
		for (int i = 0; i < candidateViews.Length; ++i) {
			int index = i;
			candidateViews [i].previousButton.onClick.AddListener (delegate {
				ChangeCandidate (index, -1);
			});
			candidateViews [i].nextButton.onClick.AddListener (delegate {
				ChangeCandidate (index, 1);
			});
		}
		confirmButton.onClick.AddListener (ConfirmButtonPressed);
		cancelButton.onClick.AddListener (BackButtonPressed);
	}

	public override void Hide (Action didHide) {
		for (int i = 0; i < candidateViews.Length; ++i) {
			candidateViews [i].previousButton.onClick.RemoveAllListeners ();
			candidateViews [i].nextButton.onClick.RemoveAllListeners ();
		}
		confirmButton.onClick.RemoveAllListeners ();
		cancelButton.onClick.RemoveAllListeners ();
		base.Hide (didHide);
	}

	void BackButtonPressed () {
		if (didFinish != null)
			didFinish (null);
	}

	void ConfirmButtonPressed () {
		var gym = GymSystem.GetCurrentGym(game, board);
		if (didFinish != null)
			didFinish (BattleFactory.CreateGymBattle(team, gym));
	}

	void ChangeCandidate (int viewIndex, int direction) {
		if (candidates.Count == candidateViews.Length)
			return;

		var view = candidateViews [viewIndex];
		int pokemonIndex = candidates.IndexOf (team[viewIndex]);
		int nextIndex = pokemonIndex;

		while (true) {
			nextIndex = (nextIndex + direction + candidates.Count) % candidates.Count;
			var nextPokemon = candidates [nextIndex];
			if (team.IndexOf (nextPokemon) == -1) {
				view.Display (nextPokemon);
				team [viewIndex] = nextPokemon;
				break;
			}
		}
	}
}