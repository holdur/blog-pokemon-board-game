﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class RandomEncounterViewController : BaseViewController {

	public enum Exits {
		Capture,
		Ignore
	}

	public Action<Exits> didFinish;
	[SerializeField] Text titleLabel;
	[SerializeField] Image avatar;
	[SerializeField] Text cpLabel;

	public void Show (Pokemon pokemon, Action didShow = null) {
		avatar.sprite = pokemon.GetAvatar();
		titleLabel.text = string.Format("A {0} has appeared!", pokemon.Entity.label);
		cpLabel.text = string.Format("CP: {0}", pokemon.CP);
		Show (didShow);
	}

	public void CaptureButtonPressed () {
		if (didFinish != null)
			didFinish(Exits.Capture);
	}

	public void IgnoreButtonPressed () {
		if (didFinish != null)
			didFinish(Exits.Ignore);
	}
}
