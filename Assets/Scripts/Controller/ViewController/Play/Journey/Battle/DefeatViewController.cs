﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DefeatViewController : BaseViewController {
	public Action didFinish;

	[SerializeField] Image avatarImage;

	void OnEnable () {
		var combatant = battle.combatants [0].mode == ControlModes.Computer ? battle.combatants [0] : battle.combatants [1];
		var pokemon =  combatant.CurrentPokemon;
		avatarImage.sprite = pokemon.GetAvatar ();
	}

	public void ContinueButtonPressed () {
		if (didFinish != null)
			didFinish ();
	}
}
