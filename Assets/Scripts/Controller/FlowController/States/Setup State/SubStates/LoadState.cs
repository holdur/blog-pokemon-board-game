﻿using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State LoadState {
		get {
			if (_loadState == null) {
				_loadState = new State(OnEnterLoadState, null, "Load");
			}
			return _loadState;
		}
	}
	State _loadState;

	void OnEnterLoadState () {
		DataController.instance.LoadGame ();
		stateMachine.ChangeState(SetupCompleteState);
	}
}
