﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State SetupState {
		get {
			if (_setupState == null)
				_setupState = new State(OnEnterSetupState, null, "Setup");
			return _setupState;
		}
	}
	State _setupState;

	void OnEnterSetupState () {
		stateMachine.ChangeState (MenuState);
	}
}
