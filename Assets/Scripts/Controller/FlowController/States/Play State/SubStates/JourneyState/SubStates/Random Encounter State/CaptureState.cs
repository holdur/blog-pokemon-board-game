﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State CaptureState {
		get {
			if (_captureState == null)
				_captureState = new State(OnEnterCaptureState, OnExitCaptureState, "Capture");
			return _captureState;
		}
	}
	State _captureState;

	void OnEnterCaptureState () {
		captureViewController.gameObject.SetActive(true);
		captureViewController.didComplete = delegate {
			stateMachine.ChangeState (EncounterCompleteState);
		};
	}

	void OnExitCaptureState () {
		captureViewController.didComplete = null;
		captureViewController.gameObject.SetActive(false);
	}
}
