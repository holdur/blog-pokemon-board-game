﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State GymCompleteState {
		get {
			if (_gymCompleteState == null)
				_gymCompleteState = new State(OnEnterGymCompleteState, null, "GymComplete");
			return _gymCompleteState;
		}
	}
	State _gymCompleteState;

	void OnEnterGymCompleteState () {
		combatViewController.gameObject.SetActive(false);
		musicController.PlayJourney (game.CurrentPlayer.badges.Count);
		stateMachine.ChangeState (CheckDestinationState);
	}

}
