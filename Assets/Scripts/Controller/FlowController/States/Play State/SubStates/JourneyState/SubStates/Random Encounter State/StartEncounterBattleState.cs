﻿using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State StartEncounterBattleState {
		get {
			if (_startEncounterBattleState == null)
				_startEncounterBattleState = new State(OnEnterStartEncounterBattleState, null, "StartEncounterBattle");
			return _startEncounterBattleState;
		}
	}
	State _startEncounterBattleState;

	void OnEnterStartEncounterBattleState () {
		musicController.PlayBattle(0);
		commandViewController.SetupEncounterBattle ();
		combatViewController.gameObject.SetActive(true);
		stateMachine.ChangeState (EncounterCommandState);
	}
}
