﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State VictoryState {
		get {
			if (_victoryState == null)
				_victoryState = new State(OnEnterVictoryState, null, "Victory");
			return _victoryState;
		}
	}
	State _victoryState;

	void OnEnterVictoryState () {
		musicController.PlayVictory (1);
		victoryViewController.Show (delegate {
			victoryViewController.didFinish = delegate {
				victoryViewController.didFinish = null;
				victoryViewController.Hide(delegate {
					stateMachine.ChangeState (GymCompleteState);
				});
			};
		});
	}

}
