﻿using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State EncounterAttackState {
		get {
			if (_encounterAttackState == null)
				_encounterAttackState = new State(OnEnterEncounterAttackState, null, "EncounterAttack");
			return _encounterAttackState;
		}
	}
	State _encounterAttackState;

	void OnEnterEncounterAttackState () {
		CombatSystem.ApplyMove(battle);
		combatViewController.didCompleteMove = delegate {
			if (battle.defender.CurrentPokemon.hitPoints == 0) {
				stateMachine.ChangeState (CaptureState);
			} else {
				stateMachine.ChangeState (EncounterCommandState);
			}
		};
		combatViewController.ApplyMove();
	}
}
