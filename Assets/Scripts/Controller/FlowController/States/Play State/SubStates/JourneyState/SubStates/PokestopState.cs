﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State PokestopState {
		get {
			if (_pokestopState == null)
				_pokestopState = new State(OnEnterPokestopState, null, "Pokestop");
			return _pokestopState;
		}
	}
	State _pokestopState;

	void OnEnterPokestopState () {
		List<SupplyAward> supplies = PokestopSystem.GetRefreshments(game, board);
		if (supplies != null) {
			var title = "You Collected:";
			var message = PokestopSystem.ToString (supplies);
			dialogViewController.Show (title, message, delegate {
				dialogViewController.didComplete = delegate {
					dialogViewController.didComplete = null;
					dialogViewController.Hide(delegate {
						stateMachine.ChangeState (RandomEncounterState);
					});
				};
			});
		} else {
			stateMachine.ChangeState (RandomEncounterState);
		}
	}
}
