﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State CheckWinState {
		get {
			if (_checkWinState == null)
				_checkWinState = new State(OnEnterCheckWinState, null, "CheckWin");
			return _checkWinState;
		}
	}
	State _checkWinState;

	void OnEnterCheckWinState () {
		if (GameSystem.IsGameOver(game)) {
			stateMachine.ChangeState (GameOverState);
		} else {
			stateMachine.ChangeState (NextPlayerState);
		}
	}
}
